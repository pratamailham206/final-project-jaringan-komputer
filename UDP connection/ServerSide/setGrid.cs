﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace server
{
    public class setGrid
    {
        protected setGrid tileAbove;
        public setGrid getTileAbove() { return this.tileAbove; }
        public void setTileAbove(setGrid newTile) { this.tileAbove = newTile; }

        protected setGrid tileBelow;
        public setGrid getTileBelow() { return this.tileBelow; }
        public void setTileBelow(setGrid newTile) { this.tileBelow = newTile; }

        protected setGrid tileToRight;
        public setGrid getTileToRight() { return this.tileToRight; }
        public void setTileToRight(setGrid newTile) { this.tileToRight = newTile; }

        protected setGrid tileToLeft;
        public setGrid getTileToLeft() { return this.tileToLeft; }
        public void setTileToLeft(setGrid newTile) { this.tileToLeft = newTile; }

        protected short X;
        public short getX() { return this.X; }
        public void setX(short newX) { this.X = newX; }

        protected short Y;
        public short getY() { return this.Y; }
        public void setY(short newY) { this.Y = newY; }

        protected bool dot;
        public bool hasDot() { return dot; }
        public void setDot(bool newDot) { this.dot = newDot; }

        protected bool superDot;
        public bool isSuperDot() { return superDot; }
        public void setSuperDot(bool newSuperDot) { this.superDot = newSuperDot; }

        protected bool wall;
        public bool isWall() { return wall; }
        public void setWall(bool newWall) { this.wall = newWall; }

        protected Image image;
        public Image getImage() { return this.image; }
        public void setImage(string path)
        {
            this.image = Image.FromFile(path);
        }
        public void setImage(Image otherImage)
        {
            this.image = otherImage;
        }

        private bool enemyTile;
        public bool isEnemyTile() { return this.enemyTile; }
        public void setEnemyTile(bool value) { this.enemyTile = value; }

        public setGrid() { }
    }
}

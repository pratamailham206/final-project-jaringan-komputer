﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization;

namespace server
{
    public class Player
    {
        protected short X;
        public short getX() { return this.X; }
        public void setX(short newX) { this.X = newX; }

        protected short Y;
        public short getY() { return this.Y; }
        public void setY(short newY) { this.Y = newY; }

        protected byte direction;
        public byte getDirection() { return this.direction; }

        protected setGrid currentTile;
        public setGrid getCurrentTile() { return this.currentTile; }
        public void setCurrentTile(setGrid newTile)
        {
            this.currentTile = newTile;
            this.X = newTile.getX();
            this.Y = newTile.getY();
        }

        public void move()
        {
            setGrid destinationTile = null;
            if (this.direction == 0)
            {
                destinationTile = this.getCurrentTile().getTileAbove();
            }
            else if (this.direction == 1)
            {
                destinationTile = this.getCurrentTile().getTileToRight();
            }
            else if (this.direction == 2)
            {
                destinationTile = this.getCurrentTile().getTileBelow();
            }
            else
            {
                destinationTile = this.getCurrentTile().getTileToLeft();
            }
            if (destinationTile != null && (!destinationTile.isWall()))
            {
                setCurrentTile(destinationTile);
            }
        }

        public Player(setGrid initialTile)
        {
            this.setCurrentTile(initialTile);
        }

        protected Player()
        {

        }


    }
}

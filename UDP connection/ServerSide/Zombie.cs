﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace server
{
    public class Zombie : Player
    {
        public Zombie(setGrid initialTile, string imagePath, byte initialDirection, bool isServer)
            : base(initialTile)
        {
            this.setImage(imagePath);
            this.setDirection(initialDirection);
            this.host = isServer;
        }

        private bool host;
        public bool isHost() { return this.host; }

        protected Image image;
        public Image getImage() { return this.image; }
        public void setImage(string imagePath)
        {
            this.image = Image.FromFile(imagePath);
            this.copyImage = (Image)this.image.Clone();
        }

        public void setImage(Image otherImage)
        {
            this.image = (Image)otherImage.Clone();
        }

        protected enemy[] target = new enemy[3];
        public enemy getTarget1() { return this.target[0]; }
        public enemy getTarget2() { return this.target[1]; }
        public enemy getTarget3() { return this.target[2]; }
        public void setTheTarget(enemy newTarget1, enemy newTarget2, enemy newTarget3)
        {
            this.target[0] = newTarget1;
            this.target[1] = newTarget2;
            this.target[2] = newTarget3;
        }

        protected Image copyImage;

        public void setDirection(byte newDirection)
        {
            this.direction = newDirection;

            this.image = (Image)this.copyImage.Clone();
            if (this.direction == 1)
            {
                this.image.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else if (this.direction == 0)
            {
                this.image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else if (this.direction == 2)
            {
                this.image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (this.direction == 3)
            {
                this.image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
        }

        protected Zombie() : base()
        {

        }

        public Zombie clone()
        {
            Zombie copy = new Zombie();
            copy.setCurrentTile(this.getCurrentTile());
            copy.setDirection(this.getDirection());
            copy.setImage(this.getImage());
            copy.setX(this.getX());
            copy.setY(this.getY());
            return copy;
        }

        public bool targetDies1()
        {
            int xDifference = getX() - target[0].getX();
            int yDifference = getY() - target[0].getY();
            return (xDifference == 0 && yDifference == 0);
        }
        public bool targetDies2()
        {
            int xDifference = getX() - target[1].getX();
            int yDifference = getY() - target[1].getY();
            return (xDifference == 0 && yDifference == 0);
        }
        public bool targetDies3()
        {
            int xDifference = getX() - target[2].getX();
            int yDifference = getY() - target[2].getY();
            return (xDifference == 0 && yDifference == 0);
        }
    }
}

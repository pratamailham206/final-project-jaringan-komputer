﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace server
{
    public partial class startConn : Form
    {
        private int player;
        private string remoteIP;
        private string remoteIP2;
        private string[] remote = new string[2];

        private Thread connectionThread;
        private UdpClient client;
        //private IPAddress ipAd = null;

        public startConn()
        {
            InitializeComponent();

        }

        private void server_Click(object sender, EventArgs e)
        {
            serverButton.Enabled = false;
            try
            {
                connectionThread = new Thread(waitForClientResponse);
                connectionThread.Start();
            }
            catch (Exception exception)
            {
                serverButton.Enabled = true;
            }
        }

        private void waitForClientResponse()
        {
            UdpClient client = new UdpClient(8080);
            IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
            IPAddress multicastAddress2 = IPAddress.Parse("239.0.0.223");

            client.JoinMulticastGroup(multicastAddress);
            client.JoinMulticastGroup(multicastAddress2);

            IPEndPoint remoteEndPoint = new IPEndPoint(multicastAddress, 12345);
            IPEndPoint remoteEndPoint2 = new IPEndPoint(multicastAddress2, 12345);

            IPEndPoint recv = null;
            IPEndPoint recv2 = null;

            byte[] data = client.Receive(ref recv);
            remote[0] = recv.Address.ToString();
            remoteIP = remote[0];

            byte[] data2 = client.Receive(ref recv2);
            remote[1] = recv2.Address.ToString();
            remoteIP2 = remote[1];

            client.Send(ASCIIEncoding.ASCII.GetBytes("JOIN_REQUEST_ACCEPTED"), 21, remoteEndPoint);

            client.Send(ASCIIEncoding.ASCII.GetBytes("JOIN_REQUEST_ACCEPTED"), 21, remoteEndPoint2);
            player = 0;
            newThreadGameStart();
            client.Close();
        }

        private void newThreadGameStart()
        {
            Thread t = new Thread(ThreadProc);
            t.Start();
        }

        private void ThreadProc()
        {
            this.Invoke((MethodInvoker)delegate () { this.Hide(); });
            Application.Run(new userInterface(player, remoteIP, remoteIP2, this));
        }

        private void MainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (connectionThread != null)
            {
                if (client != null)
                {
                    client.Close();
                }
                connectionThread.Abort();
            }
        }
    }
}

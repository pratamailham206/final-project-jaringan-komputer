﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace server
{
    public class ServerLogic : Logic
    {
        private Thread connectionThread;
        public ServerLogic(string newRemoteIP, string newRemoteIP2, userInterface gameWindow) : base(newRemoteIP, newRemoteIP2, gameWindow)
        {

        }

        public override void startGame()
        {
            base.startGame();
            connectionThread = new Thread(tick);
            connectionThread.Start();
        }

        public void tick()
        {
            int player1Win = 0;
            int player2Win = 0;
            byte qwe = 0;
            byte asd = 0;
            //byte asd = 0;
            Thread.Sleep(2000);
            while (true)
            {
                Thread.Sleep(150);

                //modify player1 direction before doing any logic
                this.gameWindow.player1.setDirection(qwe);
                this.gameWindow.player2.setDirection(asd);

                //SET PLAYER TARGET
                this.gameWindow.player1.setTheTarget(this.gameWindow.orc1, this.gameWindow.orc2, this.gameWindow.orc3);
                this.gameWindow.player2.setTheTarget(this.gameWindow.ogre1, this.gameWindow.ogre2, this.gameWindow.ogre3);


                //IF PLAYER1 KILL ENEMY
                if (this.gameWindow.player1.targetDies1())
                {
                    this.player1Score += 1;
                    player1Win++;
                }

                if (this.gameWindow.player1.targetDies2())
                {
                    this.player1Score += 1;
                    player1Win++;
                }

                if (this.gameWindow.player1.targetDies3())
                {
                    this.player1Score += 1;
                    player1Win++;
                }

                //IF PLAYER 2 KILL ENEMY
                if (this.gameWindow.player2.targetDies1())
                {
                    this.player2Score += 1;
                    player2Win++;
                }

                if (this.gameWindow.player2.targetDies2())
                {
                    this.player2Score += 1;
                    player2Win++;
                }

                if (this.gameWindow.player2.targetDies3())
                {
                    this.player2Score += 1;
                    player2Win++;
                }

                //SET ENEMY TARGET
                this.gameWindow.orc1.setTarget(this.gameWindow.player2);
                this.gameWindow.orc2.setTarget(this.gameWindow.player2);
                this.gameWindow.orc3.setTarget(this.gameWindow.player2);
                this.gameWindow.ogre1.setTarget(this.gameWindow.player1);
                this.gameWindow.ogre2.setTarget(this.gameWindow.player1);
                this.gameWindow.ogre3.setTarget(this.gameWindow.player1);

                //SEEK TARGET
                this.gameWindow.orc1.seekTarget();
                this.gameWindow.orc2.seekTarget();
                this.gameWindow.orc3.seekTarget();
                this.gameWindow.ogre1.seekTarget();
                this.gameWindow.ogre2.seekTarget();
                this.gameWindow.ogre3.seekTarget();

                //MOVE CHARACTER
                this.gameWindow.player1.move();
                this.gameWindow.player2.move();

                //MOVE ENEMY
                this.gameWindow.orc1.move();
                this.gameWindow.orc2.move();
                this.gameWindow.orc3.move();
                this.gameWindow.ogre1.move();
                this.gameWindow.ogre2.move();
                this.gameWindow.ogre3.move();

                //SET DATA
                byte[] data = new byte[47];

                //PLAYER 1
                byte[] temp = BitConverter.GetBytes(this.gameWindow.player1.getX());
                data[0] = temp[0];
                data[1] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.player1.getY());
                data[2] = temp[0];
                data[3] = temp[1];

                data[4] = (byte)this.gameWindow.player1.getDirection();

                //PLAYER 2
                temp = BitConverter.GetBytes(this.gameWindow.player2.getX());
                data[5] = temp[0];
                data[6] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.player2.getY());
                data[7] = temp[0];
                data[8] = temp[1];

                data[9] = (byte)this.gameWindow.player2.getDirection();

                //ORC 1
                temp = BitConverter.GetBytes(this.gameWindow.orc1.getX());
                data[10] = temp[0];
                data[11] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.orc1.getY());
                data[12] = temp[0];
                data[13] = temp[1];

                data[14] = (byte)this.gameWindow.orc1.getDirection();

                //ORC 2
                temp = BitConverter.GetBytes(this.gameWindow.orc2.getX());
                data[15] = temp[0];
                data[16] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.orc2.getY());
                data[17] = temp[0];
                data[18] = temp[1];

                data[19] = (byte)this.gameWindow.orc2.getDirection();

                //ORC 3
                temp = BitConverter.GetBytes(this.gameWindow.orc3.getX());
                data[20] = temp[0];
                data[21] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.orc3.getY());
                data[22] = temp[0];
                data[23] = temp[1];

                data[24] = (byte)this.gameWindow.orc3.getDirection();

                //OGRE 1
                temp = BitConverter.GetBytes(this.gameWindow.ogre1.getX());
                data[25] = temp[0];
                data[26] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.ogre1.getY());
                data[27] = temp[0];
                data[28] = temp[1];

                data[29] = (byte)this.gameWindow.ogre1.getDirection();

                //OGRE 2
                temp = BitConverter.GetBytes(this.gameWindow.ogre2.getX());
                data[30] = temp[0];
                data[31] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.ogre2.getY());
                data[32] = temp[0];
                data[33] = temp[1];

                data[34] = (byte)this.gameWindow.ogre2.getDirection();

                //OGRE 3
                temp = BitConverter.GetBytes(this.gameWindow.ogre3.getX());
                data[35] = temp[0];
                data[36] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.ogre3.getY());
                data[37] = temp[0];
                data[38] = temp[1];

                data[39] = (byte)this.gameWindow.ogre3.getDirection();

                //INITIALIZE SCORE
                temp = BitConverter.GetBytes(player1Score);
                data[40] = temp[0];
                data[41] = temp[1];

                temp = BitConverter.GetBytes(player2Score);
                data[42] = temp[0];
                data[43] = temp[1];

                data[44] = 0; // 1 if game has FIN
                data[45] = 0; // 0=PLAYER1, 1=PLAYER2, 2=DRAW :(

                if (this.gameWindow.ogre1.targetDies())
                {
                    data[44] = 1;
                    if (this.gameWindow.ogre1.getTarget().isHost())
                    {
                        data[45] = 1;
                    }
                }
                else if (this.gameWindow.ogre2.targetDies())
                {
                    data[44] = 1;
                    if (this.gameWindow.ogre2.getTarget().isHost())
                    {
                        data[45] = 1;
                    }
                }
                else if (this.gameWindow.ogre3.targetDies())
                {
                    data[44] = 1;
                    if (this.gameWindow.ogre3.getTarget().isHost())
                    {
                        data[45] = 1;
                    }
                }
                else if (this.gameWindow.orc1.targetDies())
                {
                    data[44] = 1;
                    if (this.gameWindow.orc1.getTarget().isHost())
                    {
                        data[45] = 1;
                    }
                }
                else if (this.gameWindow.orc2.targetDies())
                {
                    data[44] = 1;
                    if (this.gameWindow.orc2.getTarget().isHost())
                    {
                        data[45] = 1;
                    }
                }
                else if (this.gameWindow.orc2.targetDies())
                {
                    data[44] = 1;
                    if (this.gameWindow.orc2.getTarget().isHost())
                    {
                        data[45] = 1;
                    }
                }

                else if (player1Win == 3)
                {
                    data[44] = 1;
                    data[45] = 0;
                }

                else if (player2Win == 3)
                {
                    data[44] = 1;
                    data[45] = 1;
                }

                //DONE: TEST I
                //send data to client

                if (data[44] == 1)
                {
                    string textToShow = "";
                    if (data[45] == 0)
                    {
                        textToShow = "You have Won!!";
                    }
                    else if (data[45] == 1)
                    {
                        textToShow = "Client 1 Win. You Lost!!";
                    }
                    else if (data[45] == 2)
                    {
                        textToShow = "Client 2 Win. You Lost!!";
                    }
                    else
                    {
                        textToShow = "OMG It's a draw!";
                    }
                    Thread t = new Thread(new ParameterizedThreadStart(exit));
                    t.Start(textToShow);
                    this.gameWindow.Invoke((MethodInvoker)delegate () { this.gameWindow.unhideMainMenu(); this.gameWindow.Close(); });
                }

                bool recieved = false;
                while (!recieved)
                {
                    try
                    {
                        //SEND DATA TO CLIENT 1
                        UdpClient client = new UdpClient(getRemoteIP(), 12000);
                        UdpClient client2 = new UdpClient(getRemoteIP2(), 8000);

                        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
                        IPAddress multicastAddress2 = IPAddress.Parse("239.0.0.233");

                        client.JoinMulticastGroup(multicastAddress);
                        client2.JoinMulticastGroup(multicastAddress2);

                        IPEndPoint remoteEndPoint = new IPEndPoint(multicastAddress, 0);
                        IPEndPoint remoteEndPoint2 = new IPEndPoint(multicastAddress2, 0);

                        client.Send(data, data.Length);
                        client2.Send(data, data.Length);
                        client.Close();
                        client2.Close();

                        //Recieve state from client

                        IPEndPoint localEP = new IPEndPoint(IPAddress.Any, 12345);
                        IPEndPoint localEP2 = new IPEndPoint(IPAddress.Any, 8080);

                        client = new UdpClient(new IPEndPoint(IPAddress.Any, 12345));
                        client2 = new UdpClient(new IPEndPoint(IPAddress.Any, 8080));
                        client.JoinMulticastGroup(multicastAddress);
                        client2.JoinMulticastGroup(multicastAddress2);

                        IPEndPoint remoteEP = new IPEndPoint(multicastAddress, 0);
                        IPEndPoint remoteEP2 = new IPEndPoint(multicastAddress2, 0);

                        qwe = client.Receive(ref remoteEP)[0];
                        asd = client2.Receive(ref remoteEP2)[0];

                        client.Close();
                        client2.Close();

                        recieved = true;
                    }
                    catch (Exception e)
                    {
                        recieved = false;
                    }
                }
                if (data[45] == 1) break;
                this.OnGameStateChanged(EventArgs.Empty);
            }
        }
        public void exit(object o)
        {
            MessageBox.Show((string)o);
        }
    }
}

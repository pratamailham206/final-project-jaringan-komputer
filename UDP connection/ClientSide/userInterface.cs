﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace server
{
    public partial class userInterface : Form
    {
        public Grid grid;
        public Zombie player1;
        public Zombie player2;
        public enemy orc1;
        public enemy orc2;
        public enemy orc3;
        public enemy ogre1;
        public enemy ogre2;
        public enemy ogre3;
        public userInterface gameWindow;
        public Logic gameLogic;
        private startConn StartConnect;

        private int player1Score = 0;
        private int player2Score = 0;

        public userInterface(int player, string remoteIP, string remoteIP2, startConn StartConnect)
        {
            this.StartConnect = StartConnect;

            InitializeComponent();
            initializeGame();
            if (player == 0)
            {
                this.gameLogic = new ServerLogic(remoteIP, remoteIP2, this);
            }
            gameLogic.gameStateChange += refreshUI;
            gameLogic.startGame();
        }

        public void unhideMainMenu()
        {
            StartConnect.Invoke((MethodInvoker)delegate () { this.StartConnect.Show(); });
        }

        private void initializeGame()
        {

            grid = new Grid();
            player1 = new Zombie(grid.getTile(4, 8), "assets/server.png", 1, true);
            player2 = new Zombie(grid.getTile(11, 8), "assets/client1.png", 3, false);
            orc1 = new enemy(grid.getTile(1, 3), "assets/enemy1.png", 1, player1);
            orc2 = new enemy(grid.getTile(1, 7), "assets/enemy1.png", 1, player1);
            orc3 = new enemy(grid.getTile(1, 12), "assets/enemy1.png", 1, player1);
            ogre1 = new enemy(grid.getTile(14, 3), "assets/enemy2.png", 3, player2);
            ogre2 = new enemy(grid.getTile(14, 7), "assets/enemy2.png", 3, player2);
            ogre3 = new enemy(grid.getTile(14, 12), "assets/enemy2.png", 3, player2);

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics g = e.Graphics;
                foreach (setGrid t in grid.getTiles())
                {
                    if (t.hasDot())
                    {
                        if (t.isSuperDot())
                            g.DrawImage((Image)t.getImage().Clone(), new Point(t.getX(), t.getY()));
                        else
                            g.DrawImage((Image)t.getImage().Clone(), new Point(t.getX(), t.getY()));
                    }
                    else if (t.isWall())
                    {
                        g.DrawImage((Image)t.getImage().Clone(), new Point(t.getX(), t.getY()));
                    } 

                }
                g.DrawImage((Image)orc1.getImage().Clone(), new Point(orc1.getX() - 4, orc1.getY() - 4));
                g.DrawImage((Image)orc2.getImage().Clone(), new Point(orc2.getX() - 4, orc2.getY() - 4));
                g.DrawImage((Image)orc3.getImage().Clone(), new Point(orc3.getX() - 4, orc3.getY() - 4));
                g.DrawImage((Image)ogre1.getImage().Clone(), new Point(ogre1.getX() - 4, ogre1.getY() - 4));
                g.DrawImage((Image)ogre2.getImage().Clone(), new Point(ogre1.getX() - 4, ogre2.getY() - 4));
                g.DrawImage((Image)ogre3.getImage().Clone(), new Point(ogre1.getX() - 4, ogre3.getY() - 4));
                g.DrawImage((Image)player1.getImage().Clone(), new Point(player1.getX() - 8, player1.getY() - 8));
                g.DrawImage((Image)player2.getImage().Clone(), new Point(player2.getX() - 8, player2.getY() - 8));
                player1ScoreLabel.Text = "" + gameLogic.getPlayer1Score();
                player2ScoreLabel.Text = "" + gameLogic.getPlayer2Score2();
            }
            catch (Exception excp)
            {

            }
        }

        private void refreshUI(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up) { this.gameLogic.setDirection(0); }
            else if (e.KeyCode == Keys.Right) { this.gameLogic.setDirection(1); }
            else if (e.KeyCode == Keys.Down) { this.gameLogic.setDirection(2); }
            else if (e.KeyCode == Keys.Left) { this.gameLogic.setDirection(3); }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Up) { this.gameLogic.setDirection(0); }
            else if (e.KeyChar == (char)Keys.Right) { this.gameLogic.setDirection(0); }
            else if (e.KeyChar == (char)Keys.Down) { this.gameLogic.setDirection(0); }
            else if (e.KeyChar == (char)Keys.Left) { this.gameLogic.setDirection(0); }
        }
    }
}

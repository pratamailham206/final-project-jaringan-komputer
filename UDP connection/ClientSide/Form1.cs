﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        // var for movement
        bool moveup;
        bool movedown;
        bool moveleft;
        bool moveright;

        int speed = 5;
        int score = 0;

        private StartConn startconn;

        public Form1(string remoteIP, StartConn startconn)
        {
            this.startconn = startconn;
            InitializeComponent();

        }

        //initialize image and movement
        private void Down(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left) 
            {
                moveleft = true;
                Player.Image = Properties.Resources.Left;
            }

            if (e.KeyCode == Keys.Right) 
            {
                moveright = true;
                Player.Image = Properties.Resources.Right;
            }

                if (e.KeyCode == Keys.Up) 
            {
                moveup = true;
                Player.Image = Properties.Resources.Up;
            }

            if (e.KeyCode == Keys.Down) 
            {
                movedown = true;
                Player.Image = Properties.Resources.Down;
            }
        }

        //key movement direction
        private void Up(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left) 
            {
                moveleft = false;
            }

            if (e.KeyCode == Keys.Right) 
            {
                moveright = false;
            }

            if (e.KeyCode == Keys.Up) 
            {
                moveup = false;
            }

            if (e.KeyCode == Keys.Down) 
            {
                movedown = false;
            }
        }

        //create movement
        private void gameTimer_Tick(object sender, EventArgs e)
        {
            scoreLabel.Text = "Score: " + score;

            if (moveleft)
            {
                Player.Left -= speed;
            }

            if (moveright)
            {
                Player.Left += speed;
            }

            if (moveup)
            {
                Player.Top -= speed;
            }

            if (movedown)
            {
                Player.Top += speed;
            }

            //if player touch the border (gamover)
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag == "border")
                {
                    if (((PictureBox)x).Bounds.IntersectsWith(Player.Bounds) || score == 30)
                    {
                        Player.Left = 0;
                        label1.Text = "GAME OVER"; 
                        label1.Visible = true;
                        gameTimer.Stop();
                    }
                }
            }

            
        }

        //spawn random item
        private Area item = new Area();
        private void GenerateFood()
        {
            int maxXPos = area.Size.Width / Setting.Width;
            int maxYPos = area.Size.Height / Setting.Height;

            Random random = new Random();
            item = new Area { X = random.Next(0, maxXPos), Y = random.Next(0, maxYPos) };
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void gameBackground_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

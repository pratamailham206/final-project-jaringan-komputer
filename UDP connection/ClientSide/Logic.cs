﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace server
{
    public class Logic
    {
        public delegate void ChangedEventHandler(object sender, EventArgs e);

        public event ChangedEventHandler gameStateChange;
        protected void OnGameStateChanged(EventArgs e)
        {
            if (gameStateChange != null)
            {
                gameStateChange(this, e);
            }
        }

        protected int player1Score = 0;
        public int getPlayer1Score() { return this.player1Score; }
        protected int player2Score = 0;
        public int getPlayer2Score2() { return this.player2Score; }
        protected int player3Score = 0;
        public int getPlayer3Score() { return this.player3Score; }

        private string remoteIP;
        public string getRemoteIP() { return this.remoteIP; }
        public void setRemoteIP(string newRemoteIP) { this.remoteIP = newRemoteIP; }

        private string remoteIP2;
        public string getRemoteIP2() { return this.remoteIP2; }
        public void setRemoteIP2(string newRemoteIP2) { this.remoteIP2 = newRemoteIP2; }

        protected byte direction;
        public void setDirection(byte newDirection) { this.direction = newDirection; }
        protected userInterface gameWindow;

        public Logic(string newRemoteIP, string newRemoteIP2, userInterface gameWindow)
        {
            this.setRemoteIP(newRemoteIP);
            this.setRemoteIP2(newRemoteIP2);
            this.gameWindow = gameWindow;
        }
        public virtual void recieveDirection(object sender, EventArgs e) { }
        public virtual void startGame() { }
    }
}

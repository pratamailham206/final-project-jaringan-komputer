﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.scoreLabel = new System.Windows.Forms.Label();
            this.scorer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Player2 = new System.Windows.Forms.PictureBox();
            this.Player = new System.Windows.Forms.PictureBox();
            this.borderDown = new System.Windows.Forms.PictureBox();
            this.borderUp = new System.Windows.Forms.PictureBox();
            this.borderRight = new System.Windows.Forms.PictureBox();
            this.borderLeft = new System.Windows.Forms.PictureBox();
            this.area = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Player2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.area)).BeginInit();
            this.SuspendLayout();
            // 
            // gameTimer
            // 
            this.gameTimer.Enabled = true;
            this.gameTimer.Interval = 20;
            this.gameTimer.Tick += new System.EventHandler(this.gameTimer_Tick);
            // 
            // scoreLabel
            // 
            this.scoreLabel.AutoSize = true;
            this.scoreLabel.Font = new System.Drawing.Font("Lithos Pro Regular", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreLabel.ForeColor = System.Drawing.Color.White;
            this.scoreLabel.Location = new System.Drawing.Point(37, 23);
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(88, 23);
            this.scoreLabel.TabIndex = 1;
            this.scoreLabel.Text = "Score : ";
            // 
            // scorer
            // 
            this.scorer.AutoSize = true;
            this.scorer.Font = new System.Drawing.Font("Lithos Pro Regular", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scorer.ForeColor = System.Drawing.Color.White;
            this.scorer.Location = new System.Drawing.Point(141, 23);
            this.scorer.Name = "scorer";
            this.scorer.Size = new System.Drawing.Size(0, 23);
            this.scorer.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lithos Pro Regular", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(287, 209);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 62);
            this.label1.TabIndex = 7;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Player2
            // 
            this.Player2.Image = global::WindowsFormsApp1.Properties.Resources.f_left;
            this.Player2.Location = new System.Drawing.Point(526, 209);
            this.Player2.Name = "Player2";
            this.Player2.Size = new System.Drawing.Size(65, 76);
            this.Player2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Player2.TabIndex = 9;
            this.Player2.TabStop = false;
            // 
            // Player
            // 
            this.Player.BackColor = System.Drawing.SystemColors.Desktop;
            this.Player.Image = global::WindowsFormsApp1.Properties.Resources.Up;
            this.Player.Location = new System.Drawing.Point(168, 209);
            this.Player.Name = "Player";
            this.Player.Size = new System.Drawing.Size(65, 76);
            this.Player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Player.TabIndex = 0;
            this.Player.TabStop = false;
            // 
            // borderDown
            // 
            this.borderDown.BackColor = System.Drawing.Color.White;
            this.borderDown.Location = new System.Drawing.Point(11, 428);
            this.borderDown.Name = "borderDown";
            this.borderDown.Size = new System.Drawing.Size(765, 10);
            this.borderDown.TabIndex = 6;
            this.borderDown.TabStop = false;
            this.borderDown.Tag = "border";
            // 
            // borderUp
            // 
            this.borderUp.BackColor = System.Drawing.Color.White;
            this.borderUp.Location = new System.Drawing.Point(11, 56);
            this.borderUp.Name = "borderUp";
            this.borderUp.Size = new System.Drawing.Size(767, 10);
            this.borderUp.TabIndex = 5;
            this.borderUp.TabStop = false;
            this.borderUp.Tag = "border";
            // 
            // borderRight
            // 
            this.borderRight.BackColor = System.Drawing.Color.White;
            this.borderRight.Location = new System.Drawing.Point(767, 56);
            this.borderRight.Name = "borderRight";
            this.borderRight.Size = new System.Drawing.Size(10, 382);
            this.borderRight.TabIndex = 4;
            this.borderRight.TabStop = false;
            this.borderRight.Tag = "border";
            // 
            // borderLeft
            // 
            this.borderLeft.BackColor = System.Drawing.Color.White;
            this.borderLeft.Location = new System.Drawing.Point(11, 56);
            this.borderLeft.Name = "borderLeft";
            this.borderLeft.Size = new System.Drawing.Size(10, 382);
            this.borderLeft.TabIndex = 3;
            this.borderLeft.TabStop = false;
            this.borderLeft.Tag = "border";
            // 
            // area
            // 
            this.area.Location = new System.Drawing.Point(25, 72);
            this.area.Name = "area";
            this.area.Size = new System.Drawing.Size(736, 350);
            this.area.TabIndex = 8;
            this.area.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Player2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Player);
            this.Controls.Add(this.borderDown);
            this.Controls.Add(this.borderUp);
            this.Controls.Add(this.borderRight);
            this.Controls.Add(this.borderLeft);
            this.Controls.Add(this.scorer);
            this.Controls.Add(this.scoreLabel);
            this.Controls.Add(this.area);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Down);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Up);
            ((System.ComponentModel.ISupportInitialize)(this.Player2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.area)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Player;
        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.Label scoreLabel;
        private System.Windows.Forms.Label scorer;
        private System.Windows.Forms.PictureBox borderLeft;
        private System.Windows.Forms.PictureBox borderRight;
        private System.Windows.Forms.PictureBox borderUp;
        private System.Windows.Forms.PictureBox borderDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox area;
        private System.Windows.Forms.PictureBox Player2;
    }
}


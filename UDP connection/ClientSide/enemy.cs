﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace server
{
    public class enemy : Player
    {
        public enemy(setGrid initialTile,
            string imagePath, byte initialDirection, Zombie target)
            : base(initialTile)
        {
            this.setImage(imagePath);
            this.setDirection(initialDirection);
            this.target = target;
        }

        protected Zombie target;
        public Zombie getTarget() { return this.target; }
        public void setTarget(Zombie newTarget)
        {
            this.target = newTarget;
        }

        protected Image image;
        public Image getImage() { return this.image; }
        public void setImage(string path)
        {
            this.image = Image.FromFile(path);
            this.copyImage = (Image)this.image.Clone();
        }
        public void setImage(Image otherImage)
        {
            this.image = (Image)otherImage.Clone();
        }

        protected Image copyImage;

        public void setDirection(byte newDirection)
        {
            this.direction = newDirection;
            this.image = (Image)this.copyImage.Clone();
            if (this.direction == 1)
            {
                this.image.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else if (this.direction == 0)
            {
                this.image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else if (this.direction == 2)
            {
                this.image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (this.direction == 3)
            {
                this.image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
        }

        public void copy(enemy otherEnemy)
        {
            this.setX(otherEnemy.getX());
            this.setY(otherEnemy.getY());
            this.setDirection(otherEnemy.getDirection());
            this.setCurrentTile(otherEnemy.getCurrentTile());
        }

        protected enemy() : base()
        {

        }

        public enemy clone()
        {
            enemy copy = new enemy();
            copy.setCurrentTile(this.getCurrentTile());
            copy.setDirection(this.getDirection());
            copy.setImage(this.getImage());
            copy.setX(this.getX());
            copy.setY(this.getY());
            return copy;
        }

        public bool targetDies()
        {
            int xDifference = getX() - target.getX();
            int yDifference = getY() - target.getY();
            return (xDifference == 0 && yDifference == 0);
        }

        public void seekTarget()
        {
            if (this.getCurrentTile().isEnemyTile())
            {
                int xDifference = getX() - target.getX(); //-ve means pacman is to you're right
                int yDifference = getY() - target.getY(); //-ve means pacman is below you

                bool checkingY = true;
                bool fail = true;
                if (Math.Abs(xDifference) >= Math.Abs(yDifference))
                {
                    //move on xaxis
                    if (xDifference < 0) //target is to my right
                    {
                        if (!currentTile.getTileToRight().isWall()) // if it's not a wall
                        {
                            //I can move to right!
                            setDirection(1);
                            checkingY = false;
                            fail = false;
                        }
                    }
                    else
                    {
                        if (!currentTile.getTileToLeft().isWall()) // if it's not a wall
                        {
                            //I can move to left!
                            setDirection(3);
                            checkingY = false;
                            fail = false;
                        }
                    }
                }
                if (checkingY)
                {
                    //move on yaxis
                    if (yDifference < 0) //target is to my right
                    {
                        if (!currentTile.getTileBelow().isWall()) // if it's not a wall
                        {
                            //I can move to below!
                            setDirection(2);
                            fail = false;
                        }
                    }
                    else
                    {
                        if (!currentTile.getTileAbove().isWall()) // if it's not a wall
                        {
                            //I can move to top!
                            setDirection(0);
                            fail = false;
                        }
                    }
                }
                if (fail)
                {
                    if (direction == 0)
                    {
                        direction = 2;
                    }
                    else if (direction == 1)
                    {
                        direction = 3;
                    }
                    else if (direction == 2)
                    {
                        direction = 0;
                    }
                    else if (direction == 3)
                    {
                        direction = 1;
                    }
                }
            }
        }
    }
}

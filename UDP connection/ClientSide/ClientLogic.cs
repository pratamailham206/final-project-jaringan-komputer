﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace server
{
    public class ClientLogic : Logic
    {
        private Thread connectionThread;
        public ClientLogic(string newRemoteIP, userInterface gameWindow) : base(newRemoteIP, gameWindow)
        {

        }

        public override void startGame()
        {
            base.startGame();
            connectionThread = new Thread(tick);
            connectionThread.Start();
        }

        private void tick()
        {
            while (true)
            {
                //DONE: TEST I
                UdpClient UDPclient = new UdpClient(new IPEndPoint(IPAddress.Any, 12345));
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = UDPclient.Receive(ref remoteEndPoint);
                UDPclient.Close();

                string remoteIPint = remoteEndPoint.Address.ToString();

                //dycrypting the protocol!
                if (data[38] == 0)
                {
                    this.gameWindow.yellowPacman.setX(BitConverter.ToInt16(data, 0));
                    this.gameWindow.yellowPacman.setY(BitConverter.ToInt16(data, 2));
                    this.gameWindow.yellowPacman.setDirection(data[4]);
                    setGrid yellowTile = this.gameWindow.grid.findTile(BitConverter.ToInt16(data, 0), BitConverter.ToInt16(data, 2));
                    /*
                    if (yellowTile.hasDot())
                    {
                        //eat it
                        //yellowTile.setDot(false);
                        /*
                        //if it's a super dot, then all ghosts should change their target
                        if (yellowTile.isSuperDot())
                        {
                            string i0 = "img/ghost_yellow_up.png";
                            string i1 = "img/ghost_yellow_right.png";
                            string i2 = "img/ghost_yellow_down.png";
                            string i3 = "img/ghost_yellow_left.png";
                            this.gameWindow.ghost0.setImages(i0, i1, i2, i3);
                            this.gameWindow.ghost1.setImages(i0, i1, i2, i3);
                            this.gameWindow.ghost2.setImages(i0, i1, i2, i3);
                            this.gameWindow.ghost3.setImages(i0, i1, i2, i3);
                        }
                        
                    }
                */

                    this.gameWindow.greenPacman.setX(BitConverter.ToInt16(data, 5));
                    this.gameWindow.greenPacman.setY(BitConverter.ToInt16(data, 7));
                    this.gameWindow.greenPacman.setDirection(data[9]);

                    setGrid greenTile = this.gameWindow.grid.findTile(BitConverter.ToInt16(data, 5), BitConverter.ToInt16(data, 7));
                    /*if (greenTile.hasDot())
                    {
                        //eat it
                        //greenTile.setDot(false);
                        /*
                        //if it's a super dot, then all ghosts should change their target
                        if (greenTile.isSuperDot())
                        {
                            string i0 = "img/ghost_green_up.png";
                            string i1 = "img/ghost_green_right.png";
                            string i2 = "img/ghost_green_down.png";
                            string i3 = "img/ghost_green_left.png";
                            this.gameWindow.ghost0.setImages(i0, i1, i2, i3);
                            this.gameWindow.ghost1.setImages(i0, i1, i2, i3);
                            this.gameWindow.ghost2.setImages(i0, i1, i2, i3);
                            this.gameWindow.ghost3.setImages(i0, i1, i2, i3);
                        }
                        
                    }*/

                    this.gameWindow.yellowPacman.setX(BitConverter.ToInt16(data, 10));
                    this.gameWindow.yellowPacman.setY(BitConverter.ToInt16(data, 12));
                    this.gameWindow.yellowPacman.setDirection(data[14]);

                    this.gameWindow.greenPacman.setX(BitConverter.ToInt16(data, 15));
                    this.gameWindow.greenPacman.setY(BitConverter.ToInt16(data, 17));
                    this.gameWindow.greenPacman.setDirection(data[19]);

                    this.gameWindow.yellowPacman.setX(BitConverter.ToInt16(data, 20));
                    this.gameWindow.yellowPacman.setY(BitConverter.ToInt16(data, 22));
                    this.gameWindow.yellowPacman.setDirection(data[24]);

                    this.gameWindow.greenPacman.setX(BitConverter.ToInt16(data, 25));
                    this.gameWindow.greenPacman.setY(BitConverter.ToInt16(data, 27));
                    this.gameWindow.greenPacman.setDirection(data[29]);

                    UDPclient = new UdpClient(remoteIPint, 12345);
                    IPEndPoint dummyPoint = new IPEndPoint(IPAddress.Any, 0);
                    UDPclient.Send(new byte[] { this.direction }, 1);
                    UDPclient.Close();

                    this.OnGameStateChanged(EventArgs.Empty);
                }
                else
                {
                    string textToShow = "";
                    if (data[39] == 0)
                    {
                        textToShow = "Yellow Has won the game! You have lost, Green :-(!";
                    }
                    else if (data[39] == 1)
                    {
                        textToShow = "Green Has won the game! You have Won!";
                    }
                    else
                    {
                        textToShow = "OMG It's a draw!";
                    }
                    MessageBox.Show(textToShow);

                    this.gameWindow.Invoke((MethodInvoker)delegate () { this.gameWindow.unhideMainMenu(); this.gameWindow.Close(); });

                    break;
                }
            }
        }
    }
}

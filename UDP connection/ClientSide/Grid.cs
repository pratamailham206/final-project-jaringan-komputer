﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace server
{
    public class Grid
    {
        protected string[] textualMaze;
        public setGrid[,] tiles = new setGrid[16, 16];

        public static int totalScore = 0;

        public Grid()
        {
            textualMaze = System.IO.File.ReadAllLines("grid.txt");

            for (short i = 0; i < 16; i++)
            {
                for (short j = 0; j < 16; j++)  
                {
                    tiles[i, j] = new setGrid();          //Just initilize it
                }
            }

            for (short i = 0; i < 16; i++)        //this will be X
            {
                for (short j = 0; j < 16; j++)    //this will be Y
                {
                    char readChar = textualMaze[j][i];  //textualMaze[j] <-- up to here it gives a string
                                                        //textualMaze[j][i] <-- this is a char in a string
                                                        //Either if it's a dot (D) or a super dot (S) we care; else (W) we don't care.

                    //Creating the tile ------------------------------------------------

                    tiles[i, j].setX((short)(i * 40)); //Set x
                    tiles[i, j].setY((short)(j * 40)); //Set y

                    if (readChar == 'W')        //if it isn't a wall, it's a dot.
                    {
                        tiles[i, j].setDot(false);
                        tiles[i, j].setWall(true);
                        tiles[i, j].setImage("assets/fence.png");
                    }
                    else
                    {
                        tiles[i, j].setDot(true);
                        tiles[i, j].setWall(false);
                    }
                    if (readChar == 'D' || readChar == 'd')   
                    {
                        //totalScore += 10;

                        tiles[i, j].setImage("assets/gridDeath.png");
                    }
                    if (readChar == 'd')
                    {
                        tiles[i, j].setEnemyTile(true);
                    }
                    if (i > 0 && i < 15)         
                    {
                        tiles[i, j].setTileToLeft(tiles[i - 1, j]);
                        tiles[i, j].setTileToRight(tiles[i + 1, j]);
                    }
                    if (j > 0 && j < 15)
                    {
                        tiles[i, j].setTileAbove(tiles[i, j - 1]);
                        tiles[i, j].setTileBelow(tiles[i, j + 1]);
                    }
                }
            }
        }

        public setGrid getTile(int i, int j)
        {
            return tiles[i, j];
        }

        public setGrid findTile(int i, int j)
        {
            return tiles[i / 40, j / 40];
        }

        public setGrid[,] getTiles()
        {
            return this.tiles;
        }
    }
}
    